import json

def get_instructions():
	all_instructions = []
	with open("recipes-withsteps.json") as f:
		for line in f:
			recipe = json.loads(line)
			try: 
				all_instructions.append(recipe["instructions"])
			except KeyError:
				continue
	return all_instructions

all_instructions = get_instructions()
with open("instructions_to_parse.txt", 'w') as g:
		for instruction in all_instructions:
			g.write(instruction)




            
            
