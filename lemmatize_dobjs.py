from textblob import Word
from collections import Counter
with open("dobjs_cleaned.sm") as f:
    matrix = Counter()
    with open("dobjs_cleaned2.sm", 'w') as g:
        for line in f:
            ingredient, verb, count = line.strip().split()
            ingredient = Word(ingredient).singularize()
            verb = Word(verb).lemmatize("v")
            matrix[ingredient, verb] += int(count)
        for ing, verb in matrix:
            g.write("{} {} {}\n".format(ing, verb, matrix[ing, verb]))
