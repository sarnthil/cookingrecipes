import json
import re 

parens = re.compile(r'\(.*?\)')
possessive = re.compile(r"^.*?[']s ")
reciperegex = re.compile(r"\b(recipe|how to( make| cook| roast| handle)?|diy:?|tutorial:?|\w+ 101:?|homemade|a|(really )?easy([ -]peasy)?|quick( (&amp;|and) easy)?|best|very|everything|everyday|my|own|your|(the )?easiest( ever)?|(the )?most \w+|part|i+)\b")

all_recipes = []
raw_ingredients = {}

def filterRecipe(recipe):
	'''via gastrovec @L3vi'''
    recipe = recipe.lower()
    if recipe[0] == "(" and recipe[-1] == ")":
        recipe = recipe[1:-1]
    recipe = parens.sub("",recipe.replace("’","'"))
    recipe = possessive.sub("",recipe)
    recipe = reciperegex.sub("",recipe)
    if " with " in recipe:
      recipe = recipe.split(" with ")[0]
    if " from " in recipe:
        recipe = recipe.split(" from ")[0]
    if " - " in recipe:
        recipe = recipe.split(" - ")[0]
    recipe = " ".join(recipe.split())
    recipe = recipe.strip().replace(" ","_").replace("\t","").replace("\n","")
    return recipe

with open("recipes-withsteps.json") as f:
    for line in f:
        recipe = json.loads(line)
        filtered_name = filterRecipe(recipe["name"])
        all_recipes.append(filtered_name)
        raw_ingredients[filtered_name] = recipe["ingredients"].split("\n")

with open("raw_ingredients.json", 'w') as g:
    json.dump(raw_ingredients, g)