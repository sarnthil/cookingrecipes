#!/bin/sh

# verb  ingredient

echo "Sit back and relax. This might take a while."
LC_ALL='C' cat dobjs.txt |
perl -pe "s/^.\t/DEL/;s/^\t/DEL/;s/\t$/DEL/;s/'ve/have/g;s/^'/DEL/;s/[+*]/DEL/;s/-\t/DEL/;s/\t-/DEL/;s/^(\S+)\t(\S+)\/(\S+)$/$1\t$2\n$1\t$3/;s/\//DEL/;s/\d[^A-Za-z]/DEL/;s/^\d+//;s/\t\d+/\t/;s/^(for|in|cm|after|at|deg|ml|g|degc|c|aa|a\.)\t/DEL/" |
grep -v DEL |sort|uniq -c |
awk '{print $3 "	" $2 "	" $1}' >dobjs_cleaned.sm
echo "All done! Look at dobjs_cleaned.sm."
