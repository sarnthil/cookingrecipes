import json
import pprint
#import nltk
from collections import Counter
#from nltk.internals import find_jars_within_path
#from nltk.tokenize import RegexpTokenizer
#from nltk.tag import StanfordPOSTagger

#st = StanfordPOSTagger('english-bidirectional-distsim.tagger')
#stanford_dir = st._stanford_jar.rpartition('/')[0]
#stanford_jars = find_jars_within_path(stanford_dir)
#st._stanford_jar = ':'.join(stanford_jars)

def import_data(textfile):
    nouns = set() 
    verbs = set()
    with open(textfile) as f:
        for line in f:
            for word in line.split(" "):
                try:
                    token, tag =  word.split("_")
                    if tag.startswith("NN"):    
                        nouns.add(token)
                    if tag.startswith("VB"):
                        verbs.add(token)
                except ValueError:
                    pass
    return nouns, verbs

def save2file(lst,filename):
    with open(filename, 'w') as f:
        for word in lst:
            f.write(word + "\n")

if __name__=='__main__':
    nouns, verbs = import_data("posinstructions.txt")
    save2file(nouns, "noun_list_all.txt")
    save2file(verbs, "verb_list_all.txt")
