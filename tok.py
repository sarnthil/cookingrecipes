import re
import nltk
import nltk.tokenize
from nltk.stem import WordNetLemmatizer
from collections import defaultdict
from collections import Counter


def tokenise():
	with open ("instructions_to_parse.txt", 'r') as f:
		with open ("instructions_to_parse.tok", 'w') as out:
			for line in f:
				line = line.lower()
				lin = nltk.word_tokenize(line)
				for word in lin:
					word = word.replace(" .", "")
					out.write(word + ' ')


tokenise()


